<!DOCTYPE html>
<html>
  <head>
	<title>slothy.me</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="assets/css/bootstrap/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="assets/css/style.css" rel="stylesheet" media="screen">
	<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-43759099-1', 'slothy.me');ga('send', 'pageview');
	</script>
  </head>
  <body>
	<section class="container">
		<div class="wrap">							
			<img src="/500">
			<div class="desc">
				<h1>Slothy.me <a class="title right" href="#">random sloth</a></h1>				
				<div class="well">					
					<blockquote>Slothy.me is a place holder image service that creates random pictures of sloths at any desired size. Images are cached for four hours after it's first visit and then refreshed.</blockquote>
					<h2>Examples:</h2>
					<p><pre><strong>Square Image:</strong> &lt;img src="slothy.me/{dimension}"&gt;</pre></p>
					<p><pre><strong>Custom Size:</strong> &lt;img src="slothy.me/{width}/{height}"&gt;</pre></p>
				</div>
				<div class="well">
					<h5><strong>Pictured Here:</strong></h5>
					<p><pre>&lt;img src="slothy.me/500"&gt;</pre></p>
				</div>
			</div>
			<div class="footer">Made by Mark {{-- <a href="#">Source Code</a>--}} <span class="right">Slothy.me {{ date('Y') }}</span></div>
	  </div>
	</section>
	<script src="//code.jquery.com/jquery.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script type="text/javascript">$('.title').prop('href', '/'+Math.floor((Math.random()*50)+200));</script>
  </body>
</html>